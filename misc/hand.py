import numpy as np
import tensorflow as tf
import random

from matplotlib import cm
import matplotlib.pyplot as plt


np.random.seed(0)
tf.set_random_seed(0)

n_samples = 5000
n_samples_test = 500
portion_of_true_positive = 0.1
width = 60
height = 60

dataset = np.zeros((n_samples, width * height))
label = np.zeros((n_samples, 1))

test_dataset = np.zeros((n_samples_test, width * height))
test_label = np.zeros((n_samples_test, 1))


def rgb2gray(rgb):
    return np.dot(rgb[..., :3], [0.299, 0.587, 0.114])


prefix = "/media/tao/DATA/src/projects/HandDetector/hands"
index = 0
test_index = 0
for i in range(1, 8000, int(8000 / ((n_samples + n_samples_test) * portion_of_true_positive))):
    img = rgb2gray(plt.imread(prefix + '/h_' + str(i) + '.png'))
    if index < n_samples * portion_of_true_positive:
        dataset[index, :] = img.reshape(1, img.size)[0, 0:width * height]
        label[index, 0] = 1
        index = index + 1
    elif test_index < n_samples_test * portion_of_true_positive:
        test_dataset[test_index, :] = img.reshape(1, img.size)[0, 0:width * height]
        test_label[test_index, 0] = 1
        test_index = test_index + 1
    else:
        break


background = rgb2gray(plt.imread(prefix + '/bg_1.png'))
while True:
    sx = random.randint(0, background.shape[1] - width - 1)
    sy = random.randint(0, background.shape[0] - height - 1)
    img = background[sy:sy + height, sx:sx + width]
    if index < n_samples:
        dataset[index, :] = img.reshape(1, width * height)
        label[index, 0] = 0
        index += 1
    elif test_index < n_samples_test:
        test_dataset[test_index, :] = img.reshape(1, img.size)[0, 0:width * height]
        test_label[test_index, 0] = 0
        test_index += 1
    else:
        break

# plt.imshow(test_dataset, cmap=cm.coolwarm)
# plt.show()


def xavier_init(fan_in, fan_out, constant=1):
    """ Xavier initialization of network weights"""
    # https://stackoverflow.com/questions/33640581/how-to-do-xavier-initialization-on-tensorflow
    low = -constant * np.sqrt(6.0 / (fan_in + fan_out))
    high = constant * np.sqrt(6.0 / (fan_in + fan_out))
    return tf.random_uniform((fan_in, fan_out),
                             minval=low, maxval=high,
                             dtype=tf.float32)


class HandDetector(object):

    def __init__(self, network_architecture, transfer_fct=tf.nn.relu, learning_rate=0.001, batch_size=100):
        self.network_architecture = network_architecture
        self.transfer_fct = transfer_fct
        self.learning_rate = learning_rate
        self.batch_size = batch_size

        print network_architecture.viewkeys()
        # tf Graph input
        self.x = tf.placeholder(tf.float32, [None, network_architecture["n_input"]])
        self.y = tf.placeholder(tf.float32, [None, network_architecture["n_output"]])

        self.width = network_architecture["width"]
        self.height = network_architecture["height"]

        self._create_network()
        self._create_loss_optimizer()

        # Initializing the tensor flow variables
        init = tf.initialize_all_variables()

        # Launch the session
        self.sess = tf.InteractiveSession()
        self.sess.run(init)

    def _create_network(self):
        # Initialize autoencode network weights and biases
        self.network_weights = self._initialize_weights(**self.network_architecture)
        self.z = self._recognition_network(self.network_weights["weights_recog"], self.network_weights["biases_recog"])

    def get_weights(self):
        return self.sess.run((
            self.network_weights['weights_recog']['h1'],
            self.network_weights['weights_recog']['h2'],
            self.network_weights['weights_recog']['out_mean'],
            self.network_weights['biases_recog']['b1'],
            self.network_weights['biases_recog']['b2'],
            self.network_weights['biases_recog']['out_mean']))

    def _initialize_weights(self, n_hidden_recog_1, n_hidden_recog_2, n_input, n_output, width, height):
        all_weights = dict()
        all_weights['weights_recog'] = {
            'h1': tf.Variable(xavier_init(n_input, n_hidden_recog_1)),
            'h2': tf.Variable(xavier_init(n_hidden_recog_1, n_hidden_recog_2)),
            'out_mean': tf.Variable(xavier_init(n_hidden_recog_2, n_output))}
        all_weights['biases_recog'] = {
            'b1': tf.Variable(tf.zeros([n_hidden_recog_1], dtype=tf.float32)),
            'b2': tf.Variable(tf.zeros([n_hidden_recog_2], dtype=tf.float32)),
            'out_mean': tf.Variable(tf.zeros([n_output], dtype=tf.float32))}
        return all_weights

    def _recognition_network(self, weights, biases):
        # Generate probabilistic encoder (recognition network), which
        # maps inputs onto a normal distribution in latent space.
        # The transformation is parametrized and can be learned.
        layer_1 = self.transfer_fct(tf.add(tf.matmul(self.x, weights['h1']), biases['b1']))
        layer_2 = self.transfer_fct(tf.add(tf.matmul(layer_1, weights['h2']), biases['b2']))

        x_reconstr_mean = tf.nn.sigmoid(tf.add(tf.matmul(layer_2, weights['out_mean']), biases['out_mean']))
        return x_reconstr_mean

    def _create_loss_optimizer(self):
        cross_entropy = -(self.y * tf.log(self.z) + (1 - self.y) * tf.log(1 - self.z))
        self.loss = tf.reduce_mean(cross_entropy, name='xentropy_mean') + 0.01 * \
            tf.reduce_mean(self.network_weights['weights_recog']['h1']) + 0.01 * tf.reduce_mean(self.network_weights['weights_recog']['h2'])

        # Use ADAM optimizer
        self.optimizer = tf.train.AdagradOptimizer(learning_rate=self.learning_rate).minimize(self.loss)

    def partial_fit(self, X, Y):
        """Train model based on mini-batch of input data.

        Return loss of mini-batch.
        """
        opt, loss = self.sess.run((self.optimizer, self.loss), feed_dict={self.x: X, self.y: Y})
        return loss

    def transform(self, X):
        """Transform data by mapping it into the latent space."""
        # Note: This maps to mean of distribution, we could alternatively
        # sample from Gaussian distribution
        return self.sess.run(self.z, feed_dict={self.x: X})

    def _convolve(self, img, total_size, weights, biases):

        shape = tf.shape(img)
        filter4d = tf.reshape(weights['h1'], [self.height, self.width, 1, self.network_architecture['n_hidden_recog_1']])
        img4d = tf.expand_dims(tf.expand_dims(img, 0), 3)
        a = tf.nn.conv2d(img4d, filter4d, [1, 1, 1, 1], "SAME")
        layer_1 = self.transfer_fct(tf.add(tf.reshape(a, [total_size, self.network_architecture['n_hidden_recog_1']]), biases['b1']))
        layer_2 = self.transfer_fct(tf.add(tf.matmul(layer_1, weights['h2']), biases['b2']))

        out = tf.nn.sigmoid(tf.add(tf.matmul(layer_2, weights['out_mean']), biases['out_mean']))
        return tf.reshape(out, shape)

    def convolve(self, img):
        input_img = tf.constant(img, dtype=tf.float32)
        c = self._convolve(input_img, img.size, self.network_weights["weights_recog"], self.network_weights["biases_recog"])
        return self.sess.run(c, feed_dict={})

    def terminate(self):
        self.sess.close()


def train(network_architecture, learning_rate=0.001, batch_size=100, training_epochs=10, display_step=100):
    vae = HandDetector(network_architecture, learning_rate=learning_rate, batch_size=batch_size)

    # Training cycle
    prev_loss = 0.
    for epoch in range(training_epochs):
        avg_loss = 0.
        total_batch = int(n_samples / batch_size)
        # Loop over all batches
        for i in range(total_batch):
            batch_xs = dataset[(i) * batch_size: (i + 1) * batch_size, :]
            batch_ys = label[(i) * batch_size: (i + 1) * batch_size, :]

            # Fit training using batch data
            loss = vae.partial_fit(batch_xs, batch_ys)
            # Compute average loss
            avg_loss += loss / n_samples * batch_size

        # Display logs per epoch step
        if epoch % display_step == 0:
            print "Epoch:", '%04d' % (epoch + 1), \
                "loss=", "{:.9f}".format(avg_loss)

        if abs(avg_loss - prev_loss) < 1e-8:
            break

        prev_loss = avg_loss

    return vae


network_architecture = \
    dict(n_hidden_recog_1=5,  # 1st layer encoder neurons
         n_hidden_recog_2=5,  # 2nd layer encoder neurons
         n_input=width * height,
         n_output=1,  # dimensionality of latent space
         width=width,
         height=height)

vae_2d = train(network_architecture, training_epochs=10000)

(h1, h2, out, b1, b2, b_out) = vae_2d.get_weights()
# print h1, h2, out, b1, b2, b_out

np.save("./h1.mat", h1)
np.save("./h2.mat", h2)
np.save("./out.mat", out)
np.save("./b1.mat", b1)
np.save("./b2.mat", b2)
np.save("./b_out.mat", b_out)

Z = vae_2d.transform(test_dataset)
print "percent mismatch:", np.absolute(test_label - (Z > 0.5)).mean(axis=0) * 100

test = rgb2gray(plt.imread(prefix + '/bg_2.png'))
output_test = vae_2d.convolve(test)

plt.figure(1)
plt.imshow(test)

plt.figure(2)
plt.imshow(output_test)
plt.show()

vae_2d.terminate()
