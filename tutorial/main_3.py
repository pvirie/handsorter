import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
import random
import main_1
import sub_3


np.random.seed(0)
tf.set_random_seed(0)

n_samples = 140
width = 60
height = 60

dataset = np.zeros((n_samples, width * height))


def rgb2gray(rgb):
    return np.dot(rgb[..., :3], [0.299, 0.587, 0.114])

prefix = "/home/pvirie/Desktop/Hands_classifier/test"
index = 0
for i in range(1, n_samples, 1):
    img = rgb2gray(plt.imread(prefix + '/test (' + str(i) + ').png'))
    if index < n_samples:
        dataset[index, :] = img.reshape(1, img.size)[0, 0:width * height]
        index = index + 1
    else:
        break

ff = main_1.getNetwork()
Z1 = ff.transform(dataset)

Z2 = sub_3.transform(dataset)

theta1 = np.zeros(Z1.shape[0])
theta2 = np.zeros(Z2.shape[0])

theta1 = np.arctan2(Z1[:, 1], Z1[:, 0])
theta2 = np.arctan2(Z2[:, 1], Z2[:, 0])

indices = np.transpose(np.arange(0, n_samples, 1))

with_indices_1 = np.vstack([theta1, indices])
with_indices_2 = np.vstack([theta2, indices])

print with_indices_1, with_indices_2

with_indices_1 = with_indices_1[:, np.argsort(with_indices_1[0, :])]
with_indices_2 = with_indices_2[:, np.argsort(with_indices_2[0, :])]

print with_indices_1, with_indices_2

nx = ny = 13
canvas1 = np.empty((height * ny, width * nx))
canvas2 = np.empty((height * ny, width * nx))

x = 0
y = 0
for i in range(1, n_samples, 1):
    canvas1[y * height:(y + 1) * height, x * width:(x + 1) * width] = dataset[with_indices_1[1, i - 1], :].reshape(height, width)
    canvas2[y * height:(y + 1) * height, x * width:(x + 1) * width] = dataset[with_indices_2[1, i - 1], :].reshape(height, width)
    x = x + 1
    if x >= nx:
        x = 0
        y = y + 1

r = random.random()
print r
plt.figure(figsize=(8, 10))
plt.imshow(canvas1 if r > 0.5 else canvas2, origin="upper")
plt.tight_layout()
plt.figure(figsize=(8, 10))
plt.imshow(canvas1 if r <= 0.5 else canvas2, origin="upper")
plt.tight_layout()
plt.show()
