import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
import auc


np.random.seed(0)
tf.set_random_seed(0)

n_samples = 256
width = 60
height = 60

dataset = np.zeros((n_samples, width * height))


def rgb2gray(rgb):
    return np.dot(rgb[..., :3], [0.299, 0.587, 0.114])

prefix = "/home/pvirie/Desktop/Hands_classifier/train"
for i in range(1, n_samples):
    img = rgb2gray(plt.imread(prefix + '/train (' + str(i) + ').png'))
    dataset[i - 1, :] = img.reshape(1, img.size)[0, 0:width * height]


network_architecture = \
    dict(n_hidden_recog_1=500,  # 1st layer encoder neurons
         n_hidden_recog_2=500,  # 2nd layer encoder neurons
         n_hidden_gener_1=500,  # 1st layer decoder neurons
         n_hidden_gener_2=500,  # 2nd layer decoder neurons
         n_input=width * height,
         n_z=2)  # dimensionality of latent space

auc_2d = auc.train(dataset, n_samples, network_architecture, training_epochs=10000)

nx = ny = 20
x_values = np.linspace(-30, 30, nx)
y_values = np.linspace(-30, 30, ny)

canvas = np.empty((height * ny, width * nx))
for i, yi in enumerate(x_values):
    for j, xi in enumerate(y_values):
        z_mu = np.array([[xi, yi]])
        x_mean = auc_2d.generate(z_mu)
        canvas[(nx - i - 1) * height:(nx - i) * height, j * width:(j + 1) * width] = x_mean[0].reshape(height, width)

plt.figure(figsize=(8, 10))
Xi, Yi = np.meshgrid(x_values, y_values)
plt.imshow(canvas, origin="upper")
plt.tight_layout()
plt.show()


def getNetwork():
    return auc_2d
