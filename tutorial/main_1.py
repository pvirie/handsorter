import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
import math
import hand


np.random.seed(0)
tf.set_random_seed(0)

n_samples = 17
width = 60
height = 60

dataset = np.zeros((n_samples, width * height))
label = np.zeros((n_samples, 2))


def rgb2gray(rgb):
    return np.dot(rgb[..., :3], [0.299, 0.587, 0.114])


prefix = "/home/pvirie/Desktop/Hands_classifier/with_labels"
index = 0
with open(prefix + "/labels.txt") as f:
    for line in f:
        tokens = line.split()
        dx = float(tokens[0])
        dy = float(tokens[1])
        s = math.sqrt(dx * dx + dy * dy)
        label[index, 0] = dx / s
        label[index, 1] = dy / s
        index = index + 1

index = 0
for i in range(1, n_samples, 1):
    img = rgb2gray(plt.imread(prefix + '/label (' + str(i) + ').png'))
    if index < n_samples:
        dataset[index, :] = img.reshape(1, img.size)[0, 0:width * height]
        index = index + 1
    else:
        break


network_architecture = dict(n_hidden_recog_1=500,  # 1st layer encoder neurons
                            n_hidden_recog_2=100,  # 2nd layer encoder neurons
                            n_input=width * height,
                            n_output=2)

ff = hand.train(dataset, label, n_samples, network_architecture, training_epochs=10000)

Z = ff.transform(dataset)
print "training error:", np.square(label - Z).mean()


def getNetwork():
    return ff
