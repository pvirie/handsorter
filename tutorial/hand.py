import numpy as np
import tensorflow as tf
import math
import matplotlib.pyplot as plt


def xavier_init(fan_in, fan_out, constant=1):
    """ Xavier initialization of network weights"""
    # https://stackoverflow.com/questions/33640581/how-to-do-xavier-initialization-on-tensorflow
    low = -constant * np.sqrt(6.0 / (fan_in + fan_out))
    high = constant * np.sqrt(6.0 / (fan_in + fan_out))
    return tf.random_uniform((fan_in, fan_out),
                             minval=low, maxval=high,
                             dtype=tf.float32)


class Hands_classifier(object):

    def __init__(self, network_architecture, transfer_fct=tf.nn.sigmoid, learning_rate=0.001, batch_size=100):
        self.network_architecture = network_architecture
        self.transfer_fct = transfer_fct
        self.learning_rate = learning_rate
        self.batch_size = batch_size

        print network_architecture.viewkeys()
        # tf Graph input
        self.x = tf.placeholder(
            tf.float32, [None, network_architecture["n_input"]])
        self.y = tf.placeholder(
            tf.float32, [None, network_architecture["n_output"]])

        self._create_network()
        self._create_loss_optimizer()

        # Initializing the tensor flow variables
        init = tf.initialize_all_variables()

        # Launch the session
        self.sess = tf.InteractiveSession()
        self.sess.run(init)

    def _create_network(self):
        # Initialize autoencode network weights and biases
        self.network_weights = self._initialize_weights(
            **self.network_architecture)
        self.z = self._recognition_network(
            self.network_weights["weights_recog"], self.network_weights["biases_recog"])

    def get_weights(self):
        return self.sess.run((
            self.network_weights['weights_recog']['h1'],
            self.network_weights['weights_recog']['h2'],
            self.network_weights['weights_recog']['out_mean'],
            self.network_weights['biases_recog']['b1'],
            self.network_weights['biases_recog']['b2'],
            self.network_weights['biases_recog']['out_mean']))

    def _initialize_weights(self, n_hidden_recog_1, n_hidden_recog_2, n_input, n_output):
        all_weights = dict()
        all_weights['weights_recog'] = {
            'h1': tf.Variable(xavier_init(n_input, n_hidden_recog_1)),
            'h2': tf.Variable(xavier_init(n_hidden_recog_1, n_hidden_recog_2)),
            'out_mean': tf.Variable(xavier_init(n_hidden_recog_2, n_output))}
        all_weights['biases_recog'] = {
            'b1': tf.Variable(tf.zeros([n_hidden_recog_1], dtype=tf.float32)),
            'b2': tf.Variable(tf.zeros([n_hidden_recog_2], dtype=tf.float32)),
            'out_mean': tf.Variable(tf.zeros([n_output], dtype=tf.float32))}
        return all_weights

    def _recognition_network(self, weights, biases):
        # Generate probabilistic encoder (recognition network), which
        # maps inputs onto a normal distribution in latent space.
        # The transformation is parametrized and can be learned.
        layer_1 = self.transfer_fct(
            tf.add(tf.matmul(self.x, weights['h1']), biases['b1']))
        layer_2 = self.transfer_fct(
            tf.add(tf.matmul(layer_1, weights['h2']), biases['b2']))

        z = tf.add(tf.matmul(layer_2, weights['out_mean']), biases['out_mean'])
        return z

    def _create_loss_optimizer(self):
        lms = tf.reduce_mean(tf.square(tf.sub(self.y, self.z)))

        self.loss = lms

        # Use ADAM optimizer
        self.optimizer = tf.train.AdagradOptimizer(
            learning_rate=self.learning_rate).minimize(self.loss)

    def partial_fit(self, X, Y):
        """Train model based on mini-batch of input data.

        Return loss of mini-batch.
        """
        opt, loss = self.sess.run(
            (self.optimizer, self.loss), feed_dict={self.x: X, self.y: Y})
        return loss

    def transform(self, X):
        """Transform data by mapping it into the latent space."""
        # Note: This maps to mean of distribution, we could alternatively
        # sample from Gaussian distribution
        return self.sess.run(self.z, feed_dict={self.x: X})

    def terminate(self):
        self.sess.close()


def train(dataset, label, n_samples, network_architecture, learning_rate=0.001, batch_size=17, training_epochs=10, display_step=100):
    ff = Hands_classifier(
        network_architecture, learning_rate=learning_rate, batch_size=batch_size)

    # Training cycle
    prev_loss = 0.
    for epoch in range(training_epochs):
        avg_loss = 0.
        total_batch = int(n_samples / batch_size)
        # Loop over all batches
        for i in range(total_batch):
            batch_xs = dataset[(i) * batch_size: (i + 1) * batch_size, :]
            batch_ys = label[(i) * batch_size: (i + 1) * batch_size, :]

            # Fit training using batch data
            loss = ff.partial_fit(batch_xs, batch_ys)

            # Compute average loss
            avg_loss += loss / n_samples * batch_size

        # Display logs per epoch step
        if epoch % display_step == 0:
            print "Epoch:", '%04d' % (epoch + 1), \
                  "loss=", "{:.9f}".format(avg_loss)

        if abs(avg_loss - prev_loss) < 1e-6:
            break

        prev_loss = avg_loss

    return ff
