import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
import math
import hand
import main_2


np.random.seed(0)
tf.set_random_seed(0)

n_samples = 17
width = 60
height = 60

dataset = np.zeros((n_samples, width * height))
label = np.zeros((n_samples, 2))


def rgb2gray(rgb):
    return np.dot(rgb[..., :3], [0.299, 0.587, 0.114])


prefix = "/home/pvirie/Desktop/Hands_classifier/with_labels"
index = 0
with open(prefix + "/labels.txt") as f:
    for line in f:
        tokens = line.split()
        dx = float(tokens[0])
        dy = float(tokens[1])
        s = math.sqrt(dx * dx + dy * dy)
        label[index, 0] = dx / s
        label[index, 1] = dy / s
        index = index + 1

index = 0
for i in range(1, n_samples, 1):
    img = rgb2gray(plt.imread(prefix + '/label (' + str(i) + ').png'))
    if index < n_samples:
        dataset[index, :] = img.reshape(1, img.size)[0, 0:width * height]
        index = index + 1
    else:
        break


auc = main_2.getNetwork()
inner = auc.transform(dataset)

network_architecture = dict(n_hidden_recog_1=20,  # 1st layer encoder neurons
                            n_hidden_recog_2=20,  # 2nd layer encoder neurons
                            n_input=2,
                            n_output=2)
ff = hand.train(inner, label, n_samples, network_architecture, training_epochs=10000, learning_rate=0.1)

Z = ff.transform(inner)
print "training error:", np.square(label - Z).mean()


def transform(data):
    inner = auc.transform(data)
    out = ff.transform(inner)
    return out
