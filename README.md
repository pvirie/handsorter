# Hand sorter

A neural network with unsupervised pre-trained layers for sorting images of hand along its orientation.

## Structure

* tutorial files and data are under `/tutorial`
* scripts for generating embedding image are under `/misc`

### Blog post

<https://pvirie.wordpress.com/2016/03/29/all-about-autoencoders-2/>

### Acknowledgement

Part of this project utilizes code from [jmetzen's variational autoencoder](https://jmetzen.github.io/2015-11-27/vae.html)

### Prerequisites

TensorFlow

